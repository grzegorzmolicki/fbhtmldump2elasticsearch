package elastic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ElasticAgent {

	private RestHighLevelClient elasticSearchClient;
	private ObjectMapper        objectMapper;

	public ElasticAgent() {
		elasticSearchClient = new RestHighLevelClient(
				RestClient.builder(
						new HttpHost("localhost", 9200, "http"),
						new HttpHost("localhost", 9201, "http")));

		this.objectMapper = new ObjectMapper();
	}

	public void indexAll(Collection<?> items) {
		this.index(items.stream()
						.map(item -> {
							try {
								return objectMapper.writeValueAsString(item);
							} catch (JsonProcessingException e) {
								return null;
							}
						})
						.filter(Objects::nonNull)
						.collect(Collectors.toList()));
	}

	private void index(List<String> jsonContent) {
		BulkRequest bulkRequest = new BulkRequest();
		jsonContent.forEach(message ->
									bulkRequest.add(new IndexRequest("messages", XContentType.JSON.mediaType()).source(message, XContentType.JSON))
						   );
		try {
			this.elasticSearchClient.bulk(bulkRequest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			this.elasticSearchClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

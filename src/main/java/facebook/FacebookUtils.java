package facebook;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class FacebookUtils {

	private static final int MONTH_IDX             = 1;
	private static final int DAY_OF_MONTH_IDX      = 2;
	private static final int YEAR_IDX              = 3;
	private static final int HOUR_WITH_MINUTES_IDX = 5;
	private static final int ZONE_IDX              = 6;

	public static LocalDateTime parseDateToNonRetardedFormat(String fbLocationDateTime) {
		String[] time = fbLocationDateTime.split(" ");
		String dayString = getField(time, DAY_OF_MONTH_IDX);
		String monthString = getField(time, MONTH_IDX).substring(0, 3);
		String yearString = getField(time, YEAR_IDX);
		String hourWithMinutesString = getField(time, HOUR_WITH_MINUTES_IDX).toUpperCase();
		String zone = getField(time, ZONE_IDX);

		if (hourWithMinutesString.indexOf(":") == 1) {
			hourWithMinutesString = "0" + hourWithMinutesString;
		}

		if (dayString.length() == 1) {
			dayString = "0" + dayString;
		}

		LocalDateTime localDateTime = LocalDateTime.from(
				DateTimeFormatter.ofPattern("d MMM yyyy hh:mma", Locale.ENGLISH)
								 .parse(dayString + " " + monthString + " " + yearString + " " + hourWithMinutesString)
														);
		localDateTime.atZone(ZoneId.of(zone));

		return localDateTime;
	}

	private static String getField(String[] fbDateTimeSplit, int fieldIdx) {
		return fbDateTimeSplit[fieldIdx].replaceAll(",", "").trim();
	}
}

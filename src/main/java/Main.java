import elastic.ElasticAgent;
import facebook.FacebookUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	private static final String FB_USER = "Facebook User";

	public static void main(String... args) throws Exception {
		ElasticAgent elasticAgent = new ElasticAgent();

		long messagesCounter = 0L;
		int unknownUsersCounter = 0;
		long start = System.currentTimeMillis();
		Path messagesDirectory = Paths.get("PATH/TO/MESSAGES/DIRECTORY");

		List<Path> messageFilesPaths = Files.list(messagesDirectory)
											.filter(f -> f.getFileName().getFileName().toString().endsWith("html"))
											.collect(Collectors.toList());

		for(Path messageFilePath : messageFilesPaths) {
			File f = messageFilePath.toFile();
			try {
				Document doc = Jsoup.parse(f, "UTF-8");
				Elements conversationElements = doc.getElementsByClass("message");
				String conversationWith = doc.getElementsByTag("h3").first().text().replace("Conversation with ", "").trim();

				if (conversationWith.equals(FB_USER)) {
					conversationWith = "" + unknownUsersCounter++;
				}
				Conversation conversation = new Conversation(conversationWith);

				for (Element element : conversationElements) {

					String message = element.nextElementSibling().text();
					String author = element.children().select("span").get(0).text();
					String rawTime = element.children().select("span").get(1).text();
					LocalDateTime messageSentTime = FacebookUtils.parseDateToNonRetardedFormat(rawTime);

					Message msg = new Message(messageSentTime.format(DateTimeFormatter.ISO_LOCAL_DATE), author, message, conversationWith);
					conversation.addMessage(msg);
				}

				messagesCounter += conversation.getMessages().size();
				elasticAgent.indexAll(conversation.getMessages());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		long diff = System.currentTimeMillis() - start;
		System.out.println();
		System.out.println("Messages: " + messagesCounter);
		System.out.println("Calc took: " + diff);
		elasticAgent.close();
	}
}
